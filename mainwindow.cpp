#include "mainwindow.h"
#include "ui_mainwindow.h"

const QString MainWindow::errMsgCardWasNotChoice{"Карточка не выбрана"};
const QStringList MainWindow::carDataTableHeaders{{"Id"},{"Производитель"},{"Модель"},{"Цвет"},{"ГРЗ"},{"Владелец"},{"Контакты"},{"Проведенных работ"},{"Дата создания"},{"Дата изменения"}};
const QStringList MainWindow::repairTableHeaders{{"Id"},{"Автомобиль"},{"Дата"},{"Фото"},{"Краткое описание"}};
const QStringList MainWindow::carTypesTableHeaders{{"Id"},{"Наименование"}};

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent),
		ui(new Ui::MainWindow),
		database(this),
		repairFilter(Database::emptyId),
		userCarTypesRefreshingProc(false)

{
	ui->setupUi(this);
	carEditor = new CarEditor(this);
	repairEditor = new RepairEditor(this);


	//QTimer::singleShot(0, &database, SLOT(open()) );
	QTimer::singleShot(0, this, SLOT(checkDbExists()) );

	ui->tableWidgetCarData->horizontalHeader()->setHighlightSections(false);
	ui->tableWidgetCarData->setColumnCount( carDataTableHeaders.size() );
	ui->tableWidgetCarData->setShowGrid(true);
	ui->tableWidgetCarData->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidgetCarData->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidgetCarData->setHorizontalHeaderLabels(carDataTableHeaders);
	//ui->tableWidgetCarData->horizontalHeader()->setStretchLastSection(true);
	ui->tableWidgetCarData->setSortingEnabled(true);
	ui->tableWidgetCarData->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
	ui->tableWidgetCarData->horizontalHeader()->setSortIndicatorShown(true);
	ui->tableWidgetCarData->hideColumn(0);

	ui->tableWidgetRepairData->horizontalHeader()->setHighlightSections(false);
	ui->tableWidgetRepairData->setColumnCount( repairTableHeaders.size() );
	ui->tableWidgetRepairData->setShowGrid(true);
	ui->tableWidgetRepairData->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidgetRepairData->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidgetRepairData->setHorizontalHeaderLabels(repairTableHeaders);
	//ui->tableWidgetRepairData->horizontalHeader()->setStretchLastSection(true);
	ui->tableWidgetRepairData->setSortingEnabled(true);
	ui->tableWidgetRepairData->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
	ui->tableWidgetRepairData->horizontalHeader()->setSortIndicatorShown(true);
	ui->tableWidgetRepairData->hideColumn(0);

	ui->tableWidgetNativeCarTypes->horizontalHeader()->setHighlightSections(false);
	ui->tableWidgetNativeCarTypes->setColumnCount( carTypesTableHeaders.size() );
	ui->tableWidgetNativeCarTypes->setShowGrid(true);
	ui->tableWidgetNativeCarTypes->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidgetNativeCarTypes->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidgetNativeCarTypes->setHorizontalHeaderLabels(carTypesTableHeaders);
	ui->tableWidgetNativeCarTypes->horizontalHeader()->setStretchLastSection(true);
//	ui->tableWidgetNativeCarTypes->setSortingEnabled(true);
//	ui->tableWidgetNativeCarTypes->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
//	ui->tableWidgetNativeCarTypes->horizontalHeader()->setSortIndicatorShown(true);
	ui->tableWidgetNativeCarTypes->hideColumn(0);

	ui->tableWidgetUserCarTypes->horizontalHeader()->setHighlightSections(false);
	ui->tableWidgetUserCarTypes->setColumnCount( carTypesTableHeaders.size() );
	ui->tableWidgetUserCarTypes->setShowGrid(true);
	ui->tableWidgetUserCarTypes->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidgetUserCarTypes->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidgetUserCarTypes->setHorizontalHeaderLabels(carTypesTableHeaders);
	ui->tableWidgetUserCarTypes->horizontalHeader()->setStretchLastSection(true);
//	ui->tableWidgetUserCarTypes->setSortingEnabled(true);
//	ui->tableWidgetUserCarTypes->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
//	ui->tableWidgetUserCarTypes->horizontalHeader()->setSortIndicatorShown(true);
	ui->tableWidgetUserCarTypes->hideColumn(0);

	ui->repairFilterlineEdit->setEnabled(false);
}

//
//
//

MainWindow::~MainWindow()
{
	delete ui;
	delete carEditor;
	delete repairEditor;
}

//
//
//

void MainWindow::checkDbExists()
{
	bool firstExec = false;
	if ( !db().checkDbExists() )
	{
		firstExec = true;
		modalWarning("Первый запуск", "Будет создана база данных. Это займет немного времени.\n");
	}

	db().open();

	if (firstExec)
	{
		modalInfo("Первый запуск", "База данных успешно создана.\n");
	}
}

//
//
//

QMessageBox::StandardButton MainWindow::modalQuestion(const QString &title, const QString &text)
{
	QMessageBox messageBox( QMessageBox::Question, title, text,	QMessageBox::Yes | QMessageBox::No, this );

	messageBox.setButtonText(QMessageBox::Yes, "Да");
	messageBox.setButtonText(QMessageBox::No, "Нет");

	return static_cast<QMessageBox::StandardButton>(messageBox.exec());
}

//
//
//

void MainWindow::modalWarning(const QString &title, const QString &text)
{
	QMessageBox messageBox( QMessageBox::Warning, title, text,	QMessageBox::Ok, this );

	messageBox.setButtonText(QMessageBox::Ok, "Понятно");
	messageBox.exec();
}

//
//
//

void MainWindow::modalInfo(const QString &title, const QString &text)
{
	QMessageBox messageBox( QMessageBox::Information, title, text,	QMessageBox::Ok, this );

	messageBox.setButtonText(QMessageBox::Ok, "Понятно");
	messageBox.exec();
}

//
//
//

void MainWindow::criticalClose(QString msg)
{
	QMessageBox::critical( this, "Критическая ошибка",	msg);
	this->setEnabled(false);
}

//
//
//

void MainWindow::closeEvent(QCloseEvent *event)
{
	if ( QMessageBox::Yes == modalQuestion("Завершение работы", "Вы уверены?\n" ))
	{
		event->accept();
	}
	else
	{
		event->ignore();
	}
}

//
//
//

void MainWindow::on_newCarButton_clicked()
{
	quint32 maxCarIdx = 0;
	qint32 rowIdx = 0;

	carEditor->execWithCarId( Database::emptyId );
	refreshTableCarData();

	for ( qint32 i = 0 ; i < ui->tableWidgetCarData->rowCount() ; i++ )
	{
		if (ui->tableWidgetCarData->item( i, 0)->text().toUInt() > maxCarIdx )
		{
			maxCarIdx = ui->tableWidgetCarData->item( i, 0)->text().toUInt();
			rowIdx = i;
		}
	}

	if ( ui->tableWidgetCarData->rowCount() > 0 )
	{
		ui->tableWidgetCarData->selectRow( rowIdx );
	}
}

//
//
//

void MainWindow::on_editCarButton_clicked()
{
	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetCarData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Изменить карточку", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	carEditor->execWithCarId( ui->tableWidgetCarData->item( selectedRow, 0)->text().toUInt() );
	refreshTableCarData();
	ui->tableWidgetCarData->selectRow( selectedRow );
}

//
//
//

void MainWindow::on_previewCarDataButton_clicked()
{
	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetCarData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Просмотр карточки", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	carEditor->execWithCarId( ui->tableWidgetCarData->item( selectedRow, 0)->text().toUInt(), true );
}

//
//
//

void MainWindow::on_addRepairButton_clicked()
{
	quint32 maxRepairIdx = 0;
	qint32 repairRowIdx = 0;
	qint32 selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetCarData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Добавить карточку работ", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	repairEditor->execNewRepair( ui->tableWidgetCarData->item( selectedRow, 0)->text().toUInt() );


	refreshTableCarData();
	ui->tableWidgetCarData->selectRow( selectedRow );

	//search fresh repair entry
	on_showHistoryButton_clicked();
	for ( qint32 i = 0 ; i < ui->tableWidgetRepairData->rowCount() ; i++ )
	{
		if (ui->tableWidgetRepairData->item( i, 0)->text().toUInt() > maxRepairIdx )
		{
			maxRepairIdx = ui->tableWidgetRepairData->item( i, 0)->text().toUInt();
			repairRowIdx = i;
		}
	}

	if ( ui->tableWidgetRepairData->rowCount() > 0 )
	{
		ui->tableWidgetRepairData->selectRow( repairRowIdx );
	}

	//quint32 repairsNum = ui->tableWidgetCarData->item( selectedRow, 7)->text().toUInt();
	//ui->tableWidgetCarData->item( selectedRow, 7)->setText( QString::number( ++repairsNum ) ); //TODO: calculate col according the headers list
}

//
//
//

QString MainWindow::getShortCarStr(const Database::CarData & carData)
{
	QString retStr;

	retStr.append( db().getCarTypeById( carData.type ) );
	if ( !carData.model.isEmpty() )
		retStr.append( " " + carData.model );

	if ( !carData.color.isEmpty() )
		retStr.append( " " + carData.color );

	if ( !carData.govnumber.isEmpty() )
		retStr.append( " " + carData.govnumber );

	return retStr;
}
//
//
//

void MainWindow::on_showHistoryButton_clicked()
{
	Database::CarData carData;

	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetCarData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Просмотр истории работ", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	repairFilter = ui->tableWidgetCarData->item( selectedRow, 0)->text().toUInt();

	carData = db().getCarDataById( repairFilter );

	ui->repairFilterlineEdit->setText( getShortCarStr( carData ) );
	ui->tabWidget->setCurrentIndex(1);

	refreshTableRepair();
}

//
//
//

void MainWindow::on_removeCarButton_clicked()
{
	QString header{"Удаление карточки автомобиля"};
	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetCarData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning(header, errMsgCardWasNotChoice);
		return;
	}	

	if ( QMessageBox::Yes == modalQuestion( header,
											"Удаление карточки автомобиля приведет к удалению всех прикрепленных "
											"к ней карточек ремонта.\n"
											"Вы действительно хотите удалить карточку автомобиля?\n" ))
	{
		selectedRow = select->selectedRows().first().row();

		db().deleteCarDataById( ui->tableWidgetCarData->item( selectedRow, 0)->text().toUInt() );
		refreshTableCarData();
		refreshTableRepair();
	}
}

//
//
//

void MainWindow::on_newRepairButton_clicked()
{
	quint32 maxRepairIdx = 0;
	qint32 repairRowIdx = 0;
	if ( this->repairFilter == Database::emptyId )
	{
		modalWarning("Новая карточка работ", "Предварительно включите фильтр по карточке автомобиля");
		return;
	}

	repairEditor->execNewRepair( this->repairFilter );
	refreshTableCarData();
	refreshTableRepair();


	for ( qint32 i = 0 ; i < ui->tableWidgetRepairData->rowCount() ; i++ )
	{
		if (ui->tableWidgetRepairData->item( i, 0)->text().toUInt() > maxRepairIdx )
		{
			maxRepairIdx = ui->tableWidgetRepairData->item( i, 0)->text().toUInt();
			repairRowIdx = i;
		}
	}

	if ( ui->tableWidgetRepairData->rowCount() > 0 )
	{
		ui->tableWidgetRepairData->selectRow( repairRowIdx );
	}


}

//
//
//

void MainWindow::on_editRepairButton_clicked()
{
	Database::CarData carData;

	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetRepairData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Редактировать карточку работ", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	repairEditor->execEditOrPreviewRepair( ui->tableWidgetRepairData->item( selectedRow, 0)->text().toUInt(), RepairEditor::Mode::edit );
	refreshTableRepair();
	ui->tableWidgetRepairData->selectRow( selectedRow );
}

//
//
//

void MainWindow::on_previewRepairButton_clicked()
{
	Database::CarData carData;

	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetRepairData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Просмотр карточки работ", errMsgCardWasNotChoice);
		return;
	}

	selectedRow = select->selectedRows().first().row();
	repairEditor->execEditOrPreviewRepair( ui->tableWidgetRepairData->item( selectedRow, 0)->text().toUInt(), RepairEditor::Mode::show );
}

//
//
//

void MainWindow::on_removeRepairButton_clicked()
{
	QString header{"Удаление карточки проведенных работ"};
	Database::CarData carData;

	int selectedRow = 0;
	QItemSelectionModel *select = ui->tableWidgetRepairData->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning(header, errMsgCardWasNotChoice);
		return;
	}

	if ( QMessageBox::Yes == modalQuestion( header, "Вы действительно хотите удалить карточку проведенных работ?\n" ))
	{
		 selectedRow = select->selectedRows().first().row();
		 db().deleteRepairById( ui->tableWidgetRepairData->item( selectedRow, 0)->text().toUInt() );
		 refreshTableRepair();
	}
}

//
//
//

void MainWindow::on_clearFilterButton_clicked()
{
	int selectedRepairIdx = -1;
	QItemSelectionModel *select = ui->tableWidgetRepairData->selectionModel();

	this->repairFilter = Database::emptyId;
	this->ui->repairFilterlineEdit->setText("");

	if (select->hasSelection())
	{
		int selectedRow = select->selectedRows().first().row();
		selectedRepairIdx = ui->tableWidgetRepairData->item( selectedRow, 0)->text().toInt();
	}

	refreshTableRepair();

	if ( selectedRepairIdx == -1 )
		return;

	for ( qint32 i = 0 ; i < ui->tableWidgetRepairData->rowCount() ; i++ )
	{
		if ( ui->tableWidgetRepairData->item( i, 0)->text().toInt() == selectedRepairIdx )
		{
			ui->tableWidgetRepairData->selectRow( i );
			break;
		}
	}
}

//
//
//

bool MainWindow::isRowContains( const QTableWidget * table, const int row, const QString & searchStr )
{
	if ( row >= table->rowCount() || row < 0 )
		return false;

	for ( int col = 1 ; col < table->columnCount() ; col++ ) //searching from 1 idx because 0 coulumn is hide
	{
		if ( table->item(row, col )->text().contains( searchStr, Qt::CaseSensitivity::CaseInsensitive ) )
		{
			return true;
		}
	}

	return false;
}

//
//
//

void MainWindow::searchInTableDown(QLineEdit * lineEdit, QTableWidget * table)
{
	int selectedRow = -1;
	QItemSelectionModel *select = table->selectionModel();
	QString searchStr = lineEdit->text();

	if ( table->rowCount() == 0 || searchStr.isEmpty() )
	{
		return;
	}

	if (select->hasSelection())
	{
		selectedRow = select->selectedRows().first().row();
	}

	for( int row = selectedRow + 1 ; row < table->rowCount() ; row++ )
	{
		if ( isRowContains( table, row , searchStr ) )
		{
			table->selectRow( row );
			return;
		}
	}

	//searching process was get end of table. Try again from start
	for( int row = 0 ; row < selectedRow + 1 ; row++ )
	{
		if ( isRowContains( table, row , searchStr ) )
		{
			table->selectRow( row );
			return;
		}
	}

	table->clearSelection();
	lineEdit->setStyleSheet("color: red");
}

//
//
//

void MainWindow::searchInTableUp(QLineEdit * lineEdit, QTableWidget * table)
{
	int selectedRow = table->rowCount();
	QItemSelectionModel *select = table->selectionModel();
	QString searchStr = lineEdit->text();

	if ( table->rowCount() == 0 || searchStr.isEmpty() )
	{
		return;
	}

	if (select->hasSelection())
	{
		selectedRow = select->selectedRows().first().row();
	}

	for( int row = selectedRow - 1 ; row >= 0 ; row-- )
	{
		if ( isRowContains( table, row , searchStr ) )
		{
			table->selectRow( row );
			return;
		}
	}

	//searching process was get start of table. Try again from end
	for( int row = table->rowCount() - 1 ; row > selectedRow - 1 ; row-- )
	{
		if ( isRowContains( table, row , searchStr ) )
		{
			table->selectRow( row );
			return;
		}
	}

	table->clearSelection();
	lineEdit->setStyleSheet("color: red");
}

//
//
//

void MainWindow::on_searchCarNextButton_clicked()
{
	searchInTableDown(ui->lineEdit_searchCar, ui->tableWidgetCarData);
}

//
//
//

void MainWindow::on_searchCarPrevButton_clicked()
{
	searchInTableUp(ui->lineEdit_searchCar, ui->tableWidgetCarData);
}

//
//
//

void MainWindow::on_searchRepairPrevButton_clicked()
{
	searchInTableUp(ui->lineEdit_searchRepair, ui->tableWidgetRepairData);
}

//
//
//

void MainWindow::on_searchRepairNextButton_clicked()
{
	searchInTableDown(ui->lineEdit_searchRepair, ui->tableWidgetRepairData);
}
//
//
//

void MainWindow::refreshTableCarData()
{
	const QVector<Database::CarData> carDataVec = this->db().getCarDataAll();
	qint32 rowIdx{0};
	//ui->filterCarDataButton->isChecked()

	ui->tableWidgetCarData->setSortingEnabled(false); //switch off sorting becouse of incorrect table filling
	ui->tableWidgetCarData->setRowCount(0);

	for( const auto & carData : carDataVec )
	{
		int colIdx{0};
		ui->tableWidgetCarData->insertRow(rowIdx);

//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( QStringLiteral("%1").arg(carData.id, 12, 10, QLatin1Char('0')) ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( this->db().getCarTypeById(carData.type) ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.model ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.color ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.govnumber ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.owner_name ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.owner_phone ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( QString::number(this->db().getRepairsNumberByCarId(carData.id)) ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.dtsCreated ));
//		ui->tableWidgetCarData->setItem(rowIdx, colIdx++, new QTableWidgetItem( carData.dtsUpdated ));

		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( QStringLiteral("%1").arg(carData.id, 12, 10, QLatin1Char('0')) ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( this->db().getCarTypeById(carData.type) ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.model ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.color ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.govnumber ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.owner_name ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.owner_phone ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( QString::number(this->db().getRepairsNumberByCarId(carData.id)) ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.dtsCreated ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetCarData, rowIdx, colIdx++, new QTableWidgetItem( carData.dtsUpdated ));
		rowIdx++;
	}

	ui->tableWidgetCarData->setSortingEnabled(true);
	ui->tableWidgetCarData->resizeColumnsToContents();
	ui->lcdNumber->display(rowIdx);
}

//
//
//

void MainWindow::refreshTableRepair()
{
	QVector<Database::RepairData> repairDataVec;
	qint32 rowIdx{0};

	ui->tableWidgetRepairData->setSortingEnabled(false); //switch off sorting becouse of incorrect table filling
	ui->tableWidgetRepairData->setRowCount( rowIdx );

	if ( repairFilter == Database::emptyId )
		repairDataVec = db().getRepairsAll();
	else
		repairDataVec = db().getRepairsByCarId( repairFilter );

	for( const auto & repairData : repairDataVec )
	{
		Database::CarData carData = db().getCarDataById( repairData.carId ) ;

		//QStringList repairTableHeaders{{"Id"},{"Автомобиль"},{"Фото"},{"Дата ремонта"},{"Краткое описание"}};
		int colIdx{0};
		ui->tableWidgetRepairData->insertRow(rowIdx);

//		ui->tableWidgetRepairData->setItem(rowIdx, colIdx++, new QTableWidgetItem( QStringLiteral("%1").arg(repairData.id, 12, 10, QLatin1Char('0')) ));
//		ui->tableWidgetRepairData->setItem(rowIdx, colIdx++, new QTableWidgetItem( getShortCarStr( carData ) ));

//		ui->tableWidgetRepairData->setItem(rowIdx, colIdx++, new QTableWidgetItem( QString::number(db().getPhotoesNumberByRepairId(repairData.id)) ));
//		ui->tableWidgetRepairData->setItem(rowIdx, colIdx++, new QTableWidgetItem( repairData.dtsCreated ));
//		ui->tableWidgetRepairData->setItem(rowIdx, colIdx++, new QTableWidgetItem( repairData.header ));

		setReadOnlyQTableWidgetItem(ui->tableWidgetRepairData, rowIdx, colIdx++, new QTableWidgetItem( QStringLiteral("%1").arg(repairData.id, 12, 10, QLatin1Char('0')) ) );
		setReadOnlyQTableWidgetItem(ui->tableWidgetRepairData, rowIdx, colIdx++, new QTableWidgetItem( getShortCarStr( carData ) ));

		setReadOnlyQTableWidgetItem(ui->tableWidgetRepairData, rowIdx, colIdx++, new QTableWidgetItem( repairData.dtsCreated ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetRepairData, rowIdx, colIdx++, new QTableWidgetItem( QString::number(db().getPhotoesNumberByRepairId(repairData.id)) ));
		setReadOnlyQTableWidgetItem(ui->tableWidgetRepairData, rowIdx, colIdx++, new QTableWidgetItem( repairData.header ));
		rowIdx++;
	}

	ui->tableWidgetRepairData->setSortingEnabled(true);
	ui->tableWidgetRepairData->resizeColumnsToContents();
	ui->lcdNumber_2->display(rowIdx);
}

//
//
//

void MainWindow::refreshNativeCarTypesTables()
{
	qint32 rowIdx{0};
	auto typesList = db().getCarTypesDataList(Database::CarTypes::native);

	ui->tableWidgetNativeCarTypes->setRowCount( rowIdx );
	for( const auto & typeData : typesList )
	{
		int colIdx{0};

		if ( typeData.name != Database::defaultCarTypeNoChoise )
		{
			ui->tableWidgetNativeCarTypes->insertRow(rowIdx);
			setReadOnlyQTableWidgetItem(ui->tableWidgetNativeCarTypes, rowIdx, colIdx++, new QTableWidgetItem( typeData.id ));
			setReadOnlyQTableWidgetItem(ui->tableWidgetNativeCarTypes, rowIdx, colIdx++, new QTableWidgetItem( typeData.name ));
			rowIdx++;
		}
	}

	ui->lcdNumber_NativeCarTypes->display(rowIdx);
}

//
//
//

void MainWindow::refreshUserCarTypesTables()
{
	qint32 rowIdx{0};

	userCarData.clear();
	userCarData = db().getCarTypesDataList(Database::CarTypes::user);

	userCarTypesRefreshingProc = true;
	ui->tableWidgetUserCarTypes->setRowCount( rowIdx );
	for( const auto & typeData : userCarData )
	{
		int colIdx{0};

		if ( typeData.name != Database::defaultCarTypeNoChoise )
		{
			ui->tableWidgetUserCarTypes->insertRow(rowIdx);
			setReadOnlyQTableWidgetItem(ui->tableWidgetUserCarTypes, rowIdx, colIdx++, new QTableWidgetItem( typeData.id ));
			ui->tableWidgetUserCarTypes->setItem( rowIdx, colIdx++, new QTableWidgetItem( typeData.name ));
			rowIdx++;
		}
	}
	userCarTypesRefreshingProc = false;

	ui->lcdNumber_userCarTypes->display(rowIdx);
}

//
//
//

void MainWindow::setReadOnlyQTableWidgetItem(QTableWidget * table, int row, int col, QTableWidgetItem * item)
{
	item->setFlags(item->flags() &  ~Qt::ItemIsEditable);
	table->setItem(row, col, item );
}

//
//
//

void MainWindow::on_lineEdit_searchCar_textChanged(const QString &)
{
	if ( ui->tableWidgetCarData->rowCount() == 0 )
	{
		ui->lineEdit_searchCar->setStyleSheet("color: red");
		return;
	}

	//set style by default
	ui->lineEdit_searchCar->setStyleSheet("");
}

//
//
//

void MainWindow::on_lineEdit_searchRepair_textChanged(const QString &)
{
	if ( ui->tableWidgetRepairData->rowCount() == 0 )
	{
		ui->lineEdit_searchRepair->setStyleSheet("color: red");
		return;
	}
	//set style by default
	ui->lineEdit_searchRepair->setStyleSheet("");
}

//
//
//

void MainWindow::on_addUserCarTypeButton_clicked()
{
	int colIdx{0};
	Database::CarTypeData userCarType;
	auto rowIdx = ui->tableWidgetUserCarTypes->rowCount();

	userCarType.id = Database::emptyId;
	userCarType.name = "Новый производитель";
	userCarType.editable = true;

	userCarData.push_back(userCarType);
	ui->tableWidgetUserCarTypes->insertRow( rowIdx );
	setReadOnlyQTableWidgetItem(ui->tableWidgetUserCarTypes, rowIdx, colIdx++, new QTableWidgetItem( userCarType.id ));
	ui->tableWidgetUserCarTypes->setItem( rowIdx, colIdx++, new QTableWidgetItem( userCarType.name ));

	ui->lcdNumber_userCarTypes->display(++rowIdx);
}

//
//
//

void MainWindow::on_removeUserCarTypeButton_clicked()
{
	QItemSelectionModel *select = ui->tableWidgetUserCarTypes->selectionModel();

	if (!select->hasSelection())
	{
		modalWarning("Удаление пользовательского типа", errMsgCardWasNotChoice);
		return;
	}


	int selectedRow = select->selectedRows().first().row();
	auto carDataToDelete = std::next( userCarData.begin(), selectedRow );

	db().removeUserCarType( carDataToDelete->id );

	refreshUserCarTypesTables();
}

//
//
//

void MainWindow::on_tableWidgetUserCarTypes_cellChanged(int row, int column)
{
	if ( userCarTypesRefreshingProc || !column )
		return;

	if ( userCarData.rbegin()->id == Database::emptyId )
	{
		//save new
		userCarData.rbegin()->id = db().saveNewCarType( *userCarData.rbegin() );
	}
	else
	{
		//update
		auto carDataToUpdate = std::next( userCarData.begin(), row );

		carDataToUpdate->name = ui->tableWidgetUserCarTypes->item(row,column)->text();
		db().updateUserCarType(*carDataToUpdate);

		this->refreshTableCarData();
		this->refreshTableRepair();
	}
}


