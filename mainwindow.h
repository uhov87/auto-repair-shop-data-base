#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QCloseEvent>
#include <QAbstractButton>
#include <QTimer>
#include "careditor.h"
#include "repaireditor.h"
#include "database.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();
		virtual void closeEvent(QCloseEvent *event) override final;
		Database & db(){return database;};

		QString getShortCarStr(const Database::CarData & carData);

		QMessageBox::StandardButton modalQuestion(const QString &title, const QString &text);
		void modalWarning(const QString &title, const QString &text);
		void modalInfo(const QString &title, const QString &text);

	private slots:

		void checkDbExists();
		void criticalClose(QString msg);
		void on_newCarButton_clicked();
		void on_removeCarButton_clicked();
		void on_editCarButton_clicked();
		void on_previewCarDataButton_clicked();
		void on_addRepairButton_clicked();
		void on_showHistoryButton_clicked();

		void on_newRepairButton_clicked();
		void on_editRepairButton_clicked();
		void on_previewRepairButton_clicked();
		void on_removeRepairButton_clicked();
		void on_clearFilterButton_clicked();

		void refreshTableCarData();
		void refreshTableRepair();
		void refreshNativeCarTypesTables();
		void refreshUserCarTypesTables();

		void on_searchCarNextButton_clicked();
		void on_searchCarPrevButton_clicked();
		void on_searchRepairPrevButton_clicked();
		void on_searchRepairNextButton_clicked();

		void on_lineEdit_searchCar_textChanged(const QString &);
		void on_lineEdit_searchRepair_textChanged(const QString &);

		void on_addUserCarTypeButton_clicked();
		void on_tableWidgetUserCarTypes_cellChanged(int row, int column);

		void on_removeUserCarTypeButton_clicked();

	private:
		Ui::MainWindow *ui;
		CarEditor * carEditor;
		RepairEditor * repairEditor;
		Database database;
		quint32 repairFilter;		
		QMessageBox messageBox;

		void searchInTableDown(QLineEdit * lineEdit, QTableWidget * table);
		void searchInTableUp(QLineEdit * lineEdit, QTableWidget * table);
		void setReadOnlyQTableWidgetItem(QTableWidget * table, int row, int col, QTableWidgetItem * item);
		bool isRowContains( const QTableWidget * table, const int row, const QString & searchStr );

		QList<Database::CarTypeData> userCarData;
		bool userCarTypesRefreshingProc;

	public:
		static const QString errMsgCardWasNotChoice;
		static const QStringList carDataTableHeaders;
		static const QStringList repairTableHeaders;
		static const QStringList carTypesTableHeaders;

};
#endif // MAINWINDOW_H
