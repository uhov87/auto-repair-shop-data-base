#include "repaireditor.h"
#include "ui_repaireditor.h"

#include "mainwindow.h"
#include "photoviewer.h"

RepairEditor::RepairEditor(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::RepairEditor),
	carId(Database::emptyId),
	tableRows(0)
{
	ui->setupUi(this);
	mainwindowPtr = static_cast<MainWindow*>( parent );

	ui->lineEdit_id->setEnabled(false);
	ui->lineEdit_CarShortStr->setEnabled(false);
	ui->lineEdit_dtsCreate->setEnabled(false);
	ui->lineEdit_dtsUpdate->setEnabled(false);

	QStringList tableHeaders{{"Id"},{"Описание снимка"}};

	ui->table->horizontalHeader()->setHighlightSections(false);
	ui->table->setColumnCount( tableHeaders.size() );
	ui->table->setShowGrid(true);
	ui->table->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->table->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->table->setHorizontalHeaderLabels(tableHeaders);
	ui->table->horizontalHeader()->setStretchLastSection(true);
	//ui->table->setSortingEnabled(true);
	//ui->table->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
	//ui->table->horizontalHeader()->setSortIndicatorShown(true);
	ui->table->hideColumn(0);

	connect(ui->table->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
				 SLOT(slotSelectionChange(const QItemSelection &, const QItemSelection &))
				);

    this->setWindowFlags(Qt::Window);
}

//
//
//

RepairEditor::~RepairEditor()
{
	delete ui;
}

//
//
//

void RepairEditor::setMode(Mode mode)
{
	bool readOnlyFlag{true}, enabledFlag{false};
	this->mode = mode;

	switch(mode)
	{
		case Mode::add:
		case Mode::edit:
			readOnlyFlag = false;
			enabledFlag = true;
			break;

		default:
			break;
	}

	ui->lineEdit_CarShortStr->setReadOnly(readOnlyFlag);
	ui->lineEdit_Header->setReadOnly(readOnlyFlag);
	ui->textEdit_Desc->setReadOnly(readOnlyFlag);

	ui->saveButton->setEnabled(enabledFlag);
	ui->pushButton_addPhoto->setEnabled(enabledFlag);
	ui->pushButton_removePhoto->setEnabled(enabledFlag);
}

//
//
//

int RepairEditor::execNewRepair(const quint32 carId)
{
	Database::CarData carData = mainwindowPtr->db().getCarDataById( (this->carId = carId) );
	ui->lineEdit_CarShortStr->setText( mainwindowPtr->getShortCarStr(carData) );

	setMode(Mode::add);
	return QDialog::exec();
}

//
//
//

int RepairEditor::execEditOrPreviewRepair(const quint32 repairId, const Mode _mode_)
{
	Database::RepairData repairData = mainwindowPtr->db().getRepairsById( repairId );
	Database::CarData carData = mainwindowPtr->db().getCarDataById( ( repairData.carId) );

	ui->lineEdit_id->setText( QString::number( repairData.id ) );
	ui->lineEdit_CarShortStr->setText( mainwindowPtr->getShortCarStr(carData) );
	ui->lineEdit_Header->setText( repairData.header );
	ui->textEdit_Desc->setPlainText( repairData.desc );
	ui->lineEdit_dtsCreate->setText( repairData.dtsCreated );
	ui->lineEdit_dtsUpdate->setText( repairData.dtsUpdated );

	mainwindowPtr->db().getPhotoesDataByRepairId(repairData.id, photoDataVec);
	for ( const auto &  photoDataIt : photoDataVec)
	{
		int colIdx{0};
		ui->table->insertRow( tableRows );
		ui->table->setItem(tableRows, colIdx++, new QTableWidgetItem( QString::number(photoDataIt.id) ));

		QTableWidgetItem *item = new QTableWidgetItem;
		item->setFlags(item->flags() | Qt::ItemIsEditable);
		item->setData(Qt::DisplayRole, photoDataIt.desc);

		if (_mode_ == Mode::show)
		{
			item->setFlags(item->flags() & ~Qt::ItemIsEditable);
		}

		ui->table->setItem(tableRows, colIdx++, item);

		tableRows++;
	}

	setMode(_mode_);
	return QDialog::exec();
}

//
//
//

void RepairEditor::clearMiniature()
{
	QPixmap emptyPixmap;
	ui->picLabel->setPixmap(emptyPixmap);
}

//
//
//

void RepairEditor::slotSelectionChange(const QItemSelection &, const QItemSelection &)
{
	QPixmap pixmap;
	QModelIndexList selection = ui->table->selectionModel()->selectedRows(); //Here you are getting the indexes of the selected rows

	if (ui->table->selectionModel()->hasSelection())
	{
		pixmap.loadFromData( photoDataVec.at( selection.first().row() ).rawData );
		ui->picLabel->setPixmap(pixmap.scaled(120,120, Qt::KeepAspectRatio));
		return;
	}
}

//
//
//

void RepairEditor::on_pushButton_addPhoto_clicked()
{
	QFile * file;
	Database::PhotoData photoData;

	QString path = QFileDialog::getOpenFileName(this, "Выберите фото", "", "Снимки(*.png *.jpg *.jpeg *.bmp *.PNG *.JPG *.JPEG *.BMP)");
	int colIdx{0};

	if ( path.isEmpty() )
		return;

	photoData.id = Database::emptyId;
	photoData.repairId = Database::emptyId;
	photoData.desc = "Добавьте описание";

	file = new QFile(path);
	if (!file->open(QIODevice::ReadOnly)) {

		QMessageBox::critical(this, "Добавить фото", "Не удается открыть файл");
		return;
	}
	//pixmap = QPixmap(path);
	//ui->picLabel->setPixmap(pixmap.scaled(120,120));

	photoData.rawData = file->read(10*1024*1024);
	photoDataVec.push_back( photoData );

	ui->table->insertRow( tableRows );
	ui->table->setItem(tableRows, colIdx++, new QTableWidgetItem( QString::number(photoData.id) ));
	//ui->table->setItem(tableRows, colIdx++, new QTableWidgetItem( photoData.desc ));

	QTableWidgetItem *item = new QTableWidgetItem;
	item->setFlags(item->flags() | Qt::ItemIsEditable);
	item->setData(Qt::DisplayRole, photoData.desc);
	ui->table->setItem(tableRows, colIdx++, item);

	ui->table->selectRow( tableRows++ );
	file->close();
	delete file;
}

//
//
//

void RepairEditor::on_pushButton_removePhoto_clicked()
{	
	QString header{"Удалить фото"};
	int selectedRow = 0;
	QItemSelectionModel *select = ui->table->selectionModel();

	if (!select->hasSelection())
	{
		QMessageBox::warning(this, header, "Фото не выбрано");
		return;
	}

	if ( QMessageBox::Yes != mainwindowPtr->modalQuestion( header, "Вы уверены?\n"))
	{
		return;
	}

	selectedRow = select->selectedRows().first().row();
	ui->table->setSelectionMode(QAbstractItemView::NoSelection); //need to cancel signal selection changed

	if ( photoDataVec.at( selectedRow ).id == Database::emptyId )
	{
		;//it is new photo. Simple delete it from table
	}
	else
	{
		//delete from db
		mainwindowPtr->db().deletePhotoById( photoDataVec.at( selectedRow ).id );
	}


	photoDataVec.remove( selectedRow );
	ui->table->removeRow( selectedRow );
	tableRows--;


	clearMiniature();

	ui->table->setSelectionMode(QAbstractItemView::SingleSelection);
}

//
//
//

void RepairEditor::on_saveButton_clicked()
{
	int rowIt{0};
	Database::RepairData repairData;

	repairData.carId = this->carId;
	repairData.header = ui->lineEdit_Header->text();
	repairData.desc = ui->textEdit_Desc->toPlainText();

	if ( mode == Mode::add )
	{
		repairData.id =  mainwindowPtr->db().saveNewRapair(repairData);
	}
	else if ( mode == Mode::edit )
	{
		repairData.id = ui->lineEdit_id->text().toUInt();
		mainwindowPtr->db().updateRepairData( repairData );
	}


	for( auto & photoDataVecIt : photoDataVec )
	{
		photoDataVecIt.repairId = repairData.id ;
		photoDataVecIt.desc = ui->table->item( rowIt++, 1)->text();

		if ( photoDataVecIt.id == Database::emptyId )
		{
			mainwindowPtr->db().saveNewPhotoData(photoDataVecIt);
		}
		else
		{
			mainwindowPtr->db().updatePhotoData(photoDataVecIt);
		}
	}

	this->reject();

}

//
//
//

void RepairEditor::reject()
{
	photoDataVec.clear();
	ui->table->setRowCount( (tableRows = 0) );
	clearMiniature();

	ui->lineEdit_Header->clear();
	ui->lineEdit_dtsCreate->clear();
	ui->lineEdit_dtsUpdate->clear();
	ui->lineEdit_id->clear();
	ui->textEdit_Desc->clear();

	QDialog::reject();
}

//
//
//

void RepairEditor::on_previewButton_clicked()
{
	PhotoViewer * photoviewer;
	int selectedRow = 0;
	QItemSelectionModel *select = ui->table->selectionModel();

	if (!select->hasSelection())
	{
		QMessageBox::warning(this, "Просмотр", "Фото не выбрано");
		return;
	}

	selectedRow = select->selectedRows().first().row();
	photoviewer = new PhotoViewer(
				ui->table->item( selectedRow, 1  )->text(),
				photoDataVec.at(selectedRow).rawData,
				ui->lineEdit_CarShortStr->text() + "_" + ui->lineEdit_dtsCreate->text() + "_" + QString::number(selectedRow) + ".jpg",
				this
	);
	photoviewer->exec();
	delete photoviewer;
}
