QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    careditor.cpp \
    database.cpp \
    defaultcartypes.cpp \
    graphicsviewzoom.cpp \
    main.cpp \
    mainwindow.cpp \
    photoviewer.cpp \
    repaireditor.cpp

HEADERS += \
    careditor.h \
    database.h \
    graphicsviewzoom.h \
    mainwindow.h \
    photoviewer.h \
    repaireditor.h

FORMS += \
    careditor.ui \
    mainwindow.ui \
    photoviewer.ui \
    repaireditor.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
