#ifndef PHOTOVIEWER_H
#define PHOTOVIEWER_H

#include <QDialog>
#include <QGraphicsScene>
#include "graphicsviewzoom.h"

namespace Ui {
	class PhotoViewer;
}

class PhotoViewer : public QDialog
{
		Q_OBJECT

	public:
		explicit PhotoViewer(const QString & desc, const QByteArray & raw, const QString &exportName, QWidget *parent = nullptr);
		~PhotoViewer();

	private slots:
		void on_zoomInButton_clicked();

		void on_zoomOutButton_clicked();

		//void wheelEvent ( QWheelEvent * event );

		void on_saveButton_clicked();

	private:
		Ui::PhotoViewer *ui;
		QGraphicsScene *scene;
		QPixmap image;

		const QByteArray & raw;
		const QString & desc;
		const QString exportName;
		Graphics_view_zoom* zoom;
};

#endif // PHOTOVIEWER_H
