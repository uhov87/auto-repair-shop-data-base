#include "photoviewer.h"
#include "ui_photoviewer.h"

#include <QFileDialog>
#include <QDebug>
PhotoViewer::PhotoViewer(const QString & desc, const QByteArray & photo, const QString & exportName, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::PhotoViewer),
	raw(photo),
	desc(desc),
	exportName(exportName)
{
	ui->setupUi(this);	

	this->setWindowFlags(Qt::Window);
	this->showMaximized();

	image.loadFromData(photo);

	scene = new QGraphicsScene(this);
	scene->addPixmap(image);
	scene->setSceneRect(image.rect());

	ui->label->setText(desc);
	ui->graphicsView->setScene(scene);	

	zoom = new Graphics_view_zoom(ui->graphicsView);
	zoom->set_modifiers(Qt::NoModifier);
}

PhotoViewer::~PhotoViewer()
{
	delete zoom;
	delete scene;
	delete ui;
}



void PhotoViewer::on_zoomInButton_clicked()
{
	//this->scale = this->scale - 0.1;
	ui->graphicsView->scale(1.25, 1.25);
}

void PhotoViewer::on_zoomOutButton_clicked()
{
	//this->scale = this->scale + 0.1;
	ui->graphicsView->scale(0.8, 0.8);
}

/*
void PhotoViewer::wheelEvent ( QWheelEvent * event )
{
	qDebug() << "e";
}
*/

void PhotoViewer::on_saveButton_clicked()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Экспорт снимка",
								exportName,
								"Снимки(*.png *.jpg *.jpeg *.bmp *.PNG *.JPG *.JPEG *.BMP)");

	QFile f( fileName );
	f.open( QIODevice::WriteOnly );
	f.write(raw);
	f.close();
}
