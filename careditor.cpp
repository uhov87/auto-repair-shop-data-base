#include "careditor.h"
#include "ui_careditor.h"

#include "mainwindow.h"


CarEditor::CarEditor(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::CarEditor),
	carId(Database::emptyId)
{	
	ui->setupUi(this);
	mainwindowPtr = static_cast<MainWindow*>( parent );

	ui->lineEdit_id->setEnabled(false);
	ui->lineEdit_dtsCreate->setEnabled(false);
	ui->lineEdit_dtsUpdate->setEnabled(false);

    this->setWindowFlags(Qt::Window);
    //this->showMaximized();
}

//
//
//

CarEditor::~CarEditor()
{
	carTypesDataList.clear();
	delete ui;
}

//
//
//

int CarEditor::execWithCarId(quint32 carId, bool readonly)
{
	quint16 carTypesIdx{0};

	carTypesDataList.clear();
	//carTypesDataList = QList<Database::CarTypeData>::fromStdList(mainwindowPtr->db().getCarTypesDataList());
	carTypesDataList = mainwindowPtr->db().getCarTypesDataList();

	ui->lineEdit_id->setReadOnly( readonly );
	ui->comboBox_Type->setEnabled( !readonly );
	ui->lineEdit_model->setReadOnly( readonly );
	ui->lineEdit_color->setReadOnly( readonly );
	ui->lineEdit_govnum->setReadOnly( readonly );
	ui->lineEdit_vin->setReadOnly( readonly );
	ui->lineEdit_owner->setReadOnly( readonly );
	ui->lineEdit_contacts->setReadOnly( readonly );
	ui->textEdit_desc->setReadOnly( readonly );
	ui->lineEdit_dtsCreate->setReadOnly( readonly );
	ui->lineEdit_dtsUpdate->setReadOnly( readonly );
	ui->saveButton->setEnabled(!readonly);

	//fill combobox
	ui->comboBox_Type->clear();
	//ui->comboBox_Type->addItems( mainwindowPtr->db().getCarTypesList() );
	//ui->comboBox_Type->setItemData(0, QBrush(Qt::darkGreen), Qt::TextColorRole);


	for ( const auto & carType : carTypesDataList )
	{
		ui->comboBox_Type->addItem(carType.name);

		if ( carType.editable )
		{
			ui->comboBox_Type->setItemData(carTypesIdx, QBrush(Qt::green), Qt::TextColorRole);
		}
		else
		{
			ui->comboBox_Type->setItemData(carTypesIdx, QBrush(Qt::black), Qt::TextColorRole);
		}

		if ( carType.name == mainwindowPtr->db().defaultCarTypeNoChoise )
		{
			ui->comboBox_Type->setItemData(carTypesIdx, QBrush(Qt::red), Qt::TextColorRole);
		}

		carTypesIdx++;
	}

	this->carId = carId;	
	if ( carId != Database::emptyId )
	{
		Database::CarData carData { mainwindowPtr->db().getCarDataById(carId) };
		ui->lineEdit_id->setText( QString::number( carData.id ));
		ui->lineEdit_model->setText( carData.model );
		ui->lineEdit_color->setText( carData.color );
		ui->lineEdit_govnum->setText( carData.govnumber );
		ui->lineEdit_vin->setText( carData.vin );
		ui->lineEdit_owner->setText( carData.owner_name );
		ui->lineEdit_contacts->setText( carData.owner_phone );
		ui->textEdit_desc->setText( carData.desc );
		ui->lineEdit_dtsCreate->setText( carData.dtsCreated );
		ui->lineEdit_dtsUpdate->setText( carData.dtsUpdated );

		auto carTypesDataListTypeIt = std::find_if( carTypesDataList.begin(),
									   carTypesDataList.end(),
									   [=](const Database::CarTypeData & carType)->bool
										{
											return carType.id == (carData.type );
										});
		ui->comboBox_Type->setCurrentIndex(0);
		if ( carTypesDataListTypeIt != carTypesDataList.end() )
		{
			ui->comboBox_Type->setCurrentIndex( std::distance( carTypesDataList.begin(), carTypesDataListTypeIt ) );
		}
	}
	return QDialog::exec();
}

//
//
//

void CarEditor::on_saveButton_clicked()
{
	Database::CarData carData;

	//if carId
	//then get from db
	carData.id = carId;
	carData.model = ui->lineEdit_model->text();
	carData.color = ui->lineEdit_color->text();
	carData.govnumber = ui->lineEdit_govnum->text();
	carData.vin = ui->lineEdit_vin->text();
	carData.owner_name = ui->lineEdit_owner->text();
	carData.owner_phone = ui->lineEdit_contacts->text();
	carData.desc = ui->textEdit_desc->toPlainText();

	//carData.type = ui->comboBox_Type->currentIndex() + 1;

	auto carTypesDataListTypeIt = std::find_if( carTypesDataList.begin(),
								   carTypesDataList.end(),
								   [=](const Database::CarTypeData & carType)->bool
									{
										return carType.name == ui->comboBox_Type->currentText();
									});

	carData.type = 0;
	if ( carTypesDataListTypeIt != carTypesDataList.end() )
	{
		carData.type = carTypesDataListTypeIt->id;
	}


	if ( carId == Database::emptyId )
		mainwindowPtr->db().saveNewCarData(carData);
	else
		mainwindowPtr->db().updateCarData(carData);

	reject();
}

//
//
//

void CarEditor::reject()
{
	ui->lineEdit_id->clear();
	ui->comboBox_Type->clear();
	ui->lineEdit_model->clear();
	ui->lineEdit_color->clear();
	ui->lineEdit_govnum->clear();
	ui->lineEdit_vin->clear();
	ui->lineEdit_owner->clear();
	ui->lineEdit_contacts->clear();
	ui->textEdit_desc->clear();
	ui->lineEdit_dtsCreate->clear();
	ui->lineEdit_dtsUpdate->clear();

	carTypesDataList.clear();

	QDialog::reject();
}


