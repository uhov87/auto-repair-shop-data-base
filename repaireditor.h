#ifndef REPAIREDITOR_H
#define REPAIREDITOR_H

#include <QDialog>
#include <QFileDialog>

#include "database.h"

namespace Ui {
	class RepairEditor;
}

class MainWindow;

class RepairEditor : public QDialog
{
		Q_OBJECT

	public:
		enum class Mode
		{
			add,
			edit,
			show
		};

		explicit RepairEditor(QWidget *parent = nullptr);
		~RepairEditor();

		int execNewRepair(const quint32 carId);
		int execEditOrPreviewRepair(const quint32 repairId, const Mode mode);

	private slots:
		void on_pushButton_addPhoto_clicked();
		void slotSelectionChange(const QItemSelection &, const QItemSelection &);

		void on_pushButton_removePhoto_clicked();

		void on_saveButton_clicked();
		void clearMiniature();
		void setMode(Mode mode);

		virtual void reject() override;

		void on_previewButton_clicked();

	private:
		Ui::RepairEditor *ui;
		MainWindow * mainwindowPtr;
		quint32 carId;
		quint32 tableRows;
		QVector<Database::PhotoData> photoDataVec;

		Mode mode;


};

#endif // REPAIREDITOR_H
