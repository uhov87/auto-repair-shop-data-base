#ifndef CAREDITOR_H
#define CAREDITOR_H

#include <QDialog>
#include "database.h"

namespace Ui {
	class CarEditor;
}

class MainWindow;
class CarEditor : public QDialog
{
		Q_OBJECT

	public:
		explicit CarEditor(QWidget *parent);
		~CarEditor();

		int execWithCarId(quint32 carId, bool readonly = false);


	private slots:
		void on_saveButton_clicked();
		virtual void reject() override;

	private:
		Ui::CarEditor *ui;		
		quint32 carId;
		MainWindow * mainwindowPtr;

		QList<Database::CarTypeData> carTypesDataList;

};

#endif // CAREDITOR_H
