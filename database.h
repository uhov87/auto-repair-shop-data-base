#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql>
#include <QMessageBox>
#include <QMainWindow>

class Database : public QObject
{
	Q_OBJECT

	public:
		Database(QMainWindow * parent);
		~Database();
		bool checkDbExists();

		struct CarData
		{
			quint32 id;
			quint32 type;
			QString model;
			QString color;
			QString govnumber;
			QString vin;
			QString owner_name;
			QString owner_phone;
			QString desc;
			QString dtsCreated;
			QString dtsUpdated;
		};

		struct RepairData
		{
				//id, carId, header, desc, dts_create, dts_update
			quint32 id;
			quint32 carId;
			QString header;
			QString desc;
			QString dtsCreated;
			QString dtsUpdated;
		};

		struct PhotoData
		{
			quint32 id;
			quint32 repairId;
			QString desc;
			QByteArray rawData;
		};

		struct CarTypeData
		{
			quint32 id;
			QString name;
			bool editable;
		};

		enum class CarTypes
		{
			all,
			native,
			user
		};

		static const quint32 emptyId = 0;
		const static QStringList defaultCarTypes;
		const static QString defaultCarTypeNoChoise;

	public slots:
		bool open();
		bool createTblCarTypes();
		bool createTblCarDatas();
		bool createTblPhotos();
		bool createTblRepair();

		QString getCarTypeById( const quint32 id );
		QList<CarTypeData> getCarTypesDataList(const CarTypes carTypes = CarTypes::all);
		quint32 saveNewCarType(const Database::CarTypeData & repairData);
		void updateUserCarType(const Database::CarTypeData & userCarType);
		void removeUserCarType(const quint32 id);

		Database::CarData getCarDataById(const quint32 id);
		QVector<Database::CarData> getCarDataAll();
		void deleteCarDataById(const quint32 id);
		bool saveNewCarData(const CarData & CarData ) ;
		bool updateCarData(const CarData & CarData );

		QVector<Database::RepairData> getRepairsAll();
		Database::RepairData getRepairsById(const quint32 repairId);
		QVector<Database::RepairData> getRepairsByCarId(const quint32 carId);
		quint32 getRepairsNumberByCarId(const quint32 carId);
		quint32 saveNewRapair(const RepairData &RepairData);
		bool updateRepairData(const RepairData &repairData );
		void deleteRepairById(const quint32 id);

		void deletePhotoById( const quint32 photoId );
		void saveNewPhotoData(const Database::PhotoData & photoData);
		quint32 getPhotoesNumberByRepairId(const quint32 repairId);
		void getPhotoesDataByRepairId(const quint32 repairId, QVector<Database::PhotoData> & retValue);
		void updatePhotoData(const Database::PhotoData & photoData);

	private:

		QSqlDatabase db;
		QSqlQuery sqlQuery;

		const QString dbName{"db.sqlite3"};
		const QString reqGetLastInsertRowId{"select last_insert_rowid();"};

		const QString tblCarTypes{"car_types"};
		const QString reqCheckCarTypesPresent{"SELECT name FROM sqlite_master WHERE type='table' AND name='" + tblCarTypes +"'"};
		const QString reqCreateTblCarTypes{"CREATE TABLE " + tblCarTypes + "(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, editable INTEGER NOT NULL);"};
		const QString reqInsertIntoCarTypes{"INSERT INTO " + tblCarTypes + "(id,name,editable) VALUES(null, \"%1\", \"%2\") "};
		const QString reqInsertUpdateCarTypes{"UPDATE " + tblCarTypes + " SET name=\"%1\" WHERE id=\"%2\";"};
		const QString reqDeleteUpdateCarTypes{"DELETE FROM " + tblCarTypes + " WHERE id=%1"};

		const QString reqGetNameFromTblCarById{"SELECT name FROM " + tblCarTypes + " WHERE id=%1"};

		const QString reqGetAllCarTypes{"SELECT * FROM " + tblCarTypes};
		const QString reqGetNativeCarTypes{"SELECT * FROM " + tblCarTypes + " WHERE editable = 0;"};
		const QString reqGetUserCarTypes{"SELECT * FROM " + tblCarTypes + " WHERE editable <> 0;"};


		const QString tblCarDatas{"car_datas"};
		const QString reqCreateTblCarDatas{"CREATE TABLE IF NOT EXISTS " + tblCarDatas + "(id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER, model TEXT, color TEXT, govnumber TEXT, vin TEXT, owner_name TEXT, owner_phone TEXT, desc TEXT, dts_create TEXT, dts_update TEXT);"};
		const QString reqGetCarDataById{"SELECT * FROM " + tblCarDatas + " WHERE id=%1"};
		const QString reqDeleteCarDataById{"DELETE FROM " + tblCarDatas + " WHERE id=%1"};
		const QString reqGetCarDataAll{"SELECT * FROM " + tblCarDatas};
		const QString reqSaveNewCarData{"INSERT INTO " + tblCarDatas +
			" (id, type, model, color, govnumber, vin, owner_name, owner_phone, desc, dts_create, dts_update)"
			"VALUES(null, %1, \"%2\", \"%3\", \"%4\", \"%5\", \"%6\", \"%7\", \"%8\", "
			"datetime('now', 'localtime'), datetime('now', 'localtime'));" };
		const QString updateCarDataById{"UPDATE " + tblCarDatas + " SET "
			"type=%1, model=\"%2\", color=\"%3\", govnumber=\"%4\", vin=\"%5\", owner_name=\"%6\", owner_phone=\"%7\", desc=\"%8\", dts_update=datetime('now', 'localtime')"
			"WHERE id=%9;"};


		const QString tblRepairs{"repairs"};
		const QString reqCreateTblRepairs{"CREATE TABLE IF NOT EXISTS " + tblRepairs + "(id INTEGER PRIMARY KEY AUTOINCREMENT, carId INTEGER NOT NULL, header TEXT, desc TEXT, dts_create TEXT, dts_update TEXT, FOREIGN KEY(carId) REFERENCES " + tblCarDatas +"(id) ON DELETE CASCADE);"};
		const QString reqGetRepairsAll{"SELECT * FROM " + tblRepairs};
		const QString reqGetRepairById{"SELECT * FROM " + tblRepairs + " WHERE id=%1"};
		const QString reqGetRepairByCarId{"SELECT * FROM " + tblRepairs + " WHERE carId=%1"};
		const QString reqDeleteRepairById{"DELETE FROM " + tblRepairs + " WHERE id=%1"};
		const QString reqSaveNewRepair{"INSERT INTO " + tblRepairs + " (id, carId, header, desc, dts_create, dts_update) "
			"VALUES (null, %1, \"%2\", \"%3\", datetime('now', 'localtime'), datetime('now', 'localtime'));" };
		const QString reqUpdateRepairById{"UPDATE " + tblRepairs +	" SET header=\"%1\", desc=\"%2\", dts_update=datetime('now', 'localtime')"
			"WHERE id=%3;"};
		const QString reqGetRepairsNumberByCarId{"SELECT COUNT(*) from " + tblRepairs + " WHERE carId=%1;"};

		const QString tblPhotoes{"photoes"};
		const QString reqCreateTblPhotoes{"CREATE TABLE IF NOT EXISTS " + tblPhotoes + "(id INTEGER PRIMARY KEY AUTOINCREMENT, repairId INTEGER NOT NULL, desc TEXT, data BLOB, FOREIGN KEY(repairId) REFERENCES " + tblRepairs +"(id) ON DELETE CASCADE);"};
		const QString reqDeletePhotoById{"DELETE FROM " + tblPhotoes + " WHERE id=%1"};
		const QString reqSaveNewPhoto{"INSERT INTO " + tblPhotoes + "(id, repairId, desc, data) VALUES (null, %1, \"%2\", \"%3\");"};
		const QString reqSaveNewPhotoWithHolders{"INSERT INTO " + tblPhotoes + "(id, repairId, desc, data) VALUES (null, :1, :2, :3);"};
		const QString reqGetPhotoesNumberByRepairId{"SELECT COUNT(*) from " + tblPhotoes + " WHERE repairId=%1;"};
		const QString reqGetPhotoesByRepairId{"SELECT * from " + tblPhotoes + " WHERE repairId=%1;"};
		const QString reqUpdatePhotoById{"UPDATE " + tblPhotoes + " SET desc=\"%1\" WHERE id=%2;"};

		QMainWindow * parent;



	signals:
		void critical(QString msg);
		void refreshCarDataTable();
		void refreshRepairsDataTable();
		void refreshNativeCarTypesTables();
		void refreshUserCarTypesTables();
		//void busy(bool enable);

};

#endif // DATABASE_H
