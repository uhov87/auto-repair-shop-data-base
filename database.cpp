#include "database.h"
#include <QApplication>

Database::Database(QMainWindow * parent):
	db( QSqlDatabase::addDatabase("QSQLITE") ),
	parent(parent)
{
	connect(this, SIGNAL(critical(QString)), parent, SLOT(criticalClose(QString)));
	//connect(this, SIGNAL(busy(bool)), parent, SLOT(busy(bool)));
	connect(this, SIGNAL(refreshCarDataTable()), parent, SLOT(refreshTableCarData()));
	connect(this, SIGNAL(refreshRepairsDataTable()), parent, SLOT(refreshTableRepair()));
	connect(this, SIGNAL(refreshNativeCarTypesTables()), parent, SLOT(refreshNativeCarTypesTables()));
	connect(this, SIGNAL(refreshUserCarTypesTables()), parent, SLOT(refreshUserCarTypesTables()));


}

//
//
//

Database::~Database()
{
	db.close();
}


//
//
//

bool Database::checkDbExists()
{
	QFile dbFile{dbName};
	return dbFile.exists();
}

//
//
//

bool Database::open()
{
	db.setDatabaseName( dbName );

	if (!db.open())
	{
		emit critical( "Open\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	sqlQuery.exec("PRAGMA foreign_keys = ON;");

	if ( !createTblCarTypes() )
		return false;

	if ( !createTblCarDatas() )
		return false;

	if ( !createTblRepair() )
		return false;

	if ( !createTblPhotos() )
		return false;

	emit refreshCarDataTable();
	emit refreshRepairsDataTable();
	emit refreshNativeCarTypesTables();
	emit refreshUserCarTypesTables();

	return true;
}

//
//
//

bool Database::createTblCarTypes()
{
	QString req;

	if ( !sqlQuery.exec( reqCheckCarTypesPresent ) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	if ( !sqlQuery.first() )
	{
		if ( !sqlQuery.exec(reqCreateTblCarTypes) )
		{
			emit critical( reqCreateTblCarTypes + "\n\n" + sqlQuery.lastError().text() );
			return false;
		}

		for ( const auto & i : defaultCarTypes)
		{
			req = reqInsertIntoCarTypes.arg(i).arg(0);
			if ( !sqlQuery.exec( req ) )
			{
				emit critical( req + "\n\n" + sqlQuery.lastError().text() );
				return false;
			}
		}
	}
	return true;
}

//
//
//

bool Database::createTblCarDatas()
{
	if ( !sqlQuery.exec(reqCreateTblCarDatas) )
	{

		emit critical( reqCreateTblCarDatas + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}


	return true;
}

//
//
//

bool Database::createTblPhotos()
{
	if ( !sqlQuery.exec(reqCreateTblPhotoes) )
	{
		emit critical( reqCreateTblPhotoes + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	return true;
}

//
//
//

bool Database::createTblRepair()
{
	if ( !sqlQuery.exec(reqCreateTblRepairs) )
	{
		emit critical( reqCreateTblRepairs + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	return true;
}

//
//
//

QList<Database::CarTypeData> Database::getCarTypesDataList(const CarTypes carTypes)
//std::list<Database::CarTypeData> Database::getCarTypesDataList()
{
	Database::CarTypeData carTypeNoChoise;
	bool noChoisePresent = false;
	std::list<Database::CarTypeData> retValue;
	QString req;

	switch(carTypes)
	{
		case CarTypes::all:
			req = reqGetAllCarTypes;
			break;

		case CarTypes::native:
			req = reqGetNativeCarTypes;
			break;

		case CarTypes::user:
			req = reqGetUserCarTypes;
			break;

		default:
			return QList<Database::CarTypeData>::fromStdList(retValue);
	}

	if ( !sqlQuery.exec(req) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
		return QList<Database::CarTypeData>::fromStdList(retValue);
		//return retValue;
	}

	while (sqlQuery.next())
	{
		Database::CarTypeData carType;

		carType.name = sqlQuery.value( sqlQuery.record().indexOf("name")).toString() ;
		carType.id = sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt();
		carType.editable = sqlQuery.value( sqlQuery.record().indexOf("editable")).toBool();

		if ( carType.name == defaultCarTypeNoChoise && !noChoisePresent )
		{
			carTypeNoChoise = carType;
			noChoisePresent = true;
		}
		else
		{
			retValue.push_back( carType );
		}
	}

	retValue.sort(	[]
					( const Database::CarTypeData & a, const Database::CarTypeData & b )->bool
					{
						return ( QString::compare(a.name, b.name, Qt::CaseInsensitive) > 0 ) ? false : true;
					}
	);

	if ( noChoisePresent )
		retValue.push_front( carTypeNoChoise );

	return QList<Database::CarTypeData>::fromStdList(retValue);
	//return retValue;
}

//
//
//

quint32 Database::saveNewCarType(const Database::CarTypeData & userCarType)
{
	quint32 ret{0};

	if ( !sqlQuery.exec( reqInsertIntoCarTypes.arg( userCarType.name ).arg(1)) )
	{
		emit critical( reqInsertIntoCarTypes.arg( userCarType.name ).arg(1) + \
					   "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	if ( !sqlQuery.exec( reqGetLastInsertRowId ) )
	{
		emit critical( reqGetLastInsertRowId + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		ret	= sqlQuery.value( sqlQuery.record().indexOf("last_insert_rowid()")).toUInt() ;
	}

	return ret;
}

//
//
//

void Database::updateUserCarType(const Database::CarTypeData & userCarType)
{


	if ( !sqlQuery.exec( reqInsertUpdateCarTypes.arg( userCarType.name ).arg( userCarType.id )) )
	{
		emit critical( reqInsertUpdateCarTypes.arg( userCarType.name ).arg( userCarType.id ) + \
					   "\n\n" + sqlQuery.lastError().text() );
	}

	return;
}

//
//
//

void Database::removeUserCarType(const quint32 id)
{
	if ( !sqlQuery.exec( reqDeleteUpdateCarTypes.arg( id )) )
	{
		emit critical( reqDeleteUpdateCarTypes.arg( id ) + \
					   "\n\n" + sqlQuery.lastError().text() );
	}

	return;
}

//
//
//

QString Database::getCarTypeById(const quint32 id)
{
	QString retVal = defaultCarTypeNoChoise;

	if ( !sqlQuery.exec( reqGetNameFromTblCarById.arg(id) ) )
	{
		emit critical( reqGetNameFromTblCarById.arg(id) + "\n\n" + sqlQuery.lastError().text() );
		return retVal;
	}

	if ( sqlQuery.first() )
	{
		retVal = sqlQuery.value( sqlQuery.record().indexOf("name") ).toString();
		return retVal;
	}
	return retVal;
}

//
//
//

Database::CarData Database::getCarDataById(const quint32 id)
{
	Database::CarData ret;

	if ( !sqlQuery.exec( reqGetCarDataById.arg(id) ) )
	{
		emit critical( reqGetCarDataById.arg(id) + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		ret.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		ret.type		= sqlQuery.value( sqlQuery.record().indexOf("type")).toUInt() ;
		ret.model		= sqlQuery.value( sqlQuery.record().indexOf("model")).toString() ;
		ret.color		= sqlQuery.value( sqlQuery.record().indexOf("color")).toString() ;
		ret.govnumber	= sqlQuery.value( sqlQuery.record().indexOf("govnumber")).toString() ;
		ret.vin			= sqlQuery.value( sqlQuery.record().indexOf("vin")).toString() ;
		ret.owner_name	= sqlQuery.value( sqlQuery.record().indexOf("owner_name")).toString() ;
		ret.owner_phone	= sqlQuery.value( sqlQuery.record().indexOf("owner_phone")).toString() ;
		ret.desc		= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		ret.dtsCreated	= sqlQuery.value( sqlQuery.record().indexOf("dts_create")).toString() ;
		ret.dtsUpdated	= sqlQuery.value( sqlQuery.record().indexOf("dts_update")).toString() ;
	}
	return ret;
}

//
//
//

QVector<Database::CarData> Database::getCarDataAll()
{
	QVector<Database::CarData> ret;

	if ( !sqlQuery.exec(reqGetCarDataAll) )
	{
		emit critical( reqGetCarDataAll + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		Database::CarData carData;

		carData.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		carData.type		= sqlQuery.value( sqlQuery.record().indexOf("type")).toUInt() ;
		carData.model		= sqlQuery.value( sqlQuery.record().indexOf("model")).toString() ;
		carData.color		= sqlQuery.value( sqlQuery.record().indexOf("color")).toString() ;
		carData.govnumber	= sqlQuery.value( sqlQuery.record().indexOf("govnumber")).toString() ;
		carData.vin			= sqlQuery.value( sqlQuery.record().indexOf("vin")).toString() ;
		carData.owner_name	= sqlQuery.value( sqlQuery.record().indexOf("owner_name")).toString() ;
		carData.owner_phone	= sqlQuery.value( sqlQuery.record().indexOf("owner_phone")).toString() ;
		carData.desc		= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		carData.dtsCreated	= sqlQuery.value( sqlQuery.record().indexOf("dts_create")).toString() ;
		carData.dtsUpdated	= sqlQuery.value( sqlQuery.record().indexOf("dts_update")).toString() ;

		ret.push_back( carData );
	}

	return ret;
}

//
//
//

void Database::deleteCarDataById(const quint32 id)
{

	if ( !sqlQuery.exec( reqDeleteCarDataById.arg(id) ) )
	{
		emit critical( reqDeleteCarDataById.arg(id) + "\n\n" + sqlQuery.lastError().text() );
	}
}

//
//
//

bool Database::saveNewCarData( const Database::CarData & carData )
{
	QString req = reqSaveNewCarData.arg( carData.type )
									.arg( carData.model )
									.arg( carData.color )
									.arg( carData.govnumber )
									.arg( carData.vin )
									.arg( carData.owner_name )
									.arg( carData.owner_phone )
									.arg( carData.desc );

	if ( !sqlQuery.exec(req) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}
	return true;
}

//
//
//

bool Database::updateCarData( const Database::CarData & carData )
{
	QString req = updateCarDataById.arg( carData.type )
									.arg( carData.model )
									.arg( carData.color )
									.arg( carData.govnumber )
									.arg( carData.vin )
									.arg( carData.owner_name )
									.arg( carData.owner_phone )
									.arg( carData.desc )
									.arg( carData.id);

	if ( !sqlQuery.exec(req) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	return true;
}

//
//
//

QVector<Database::RepairData> Database::getRepairsAll()
{
	QVector<Database::RepairData> ret;

	if ( !sqlQuery.exec(reqGetRepairsAll) )
	{
		emit critical( reqGetRepairsAll + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		Database::RepairData repairData;
		repairData.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		repairData.carId		= sqlQuery.value( sqlQuery.record().indexOf("carId")).toUInt() ;
		repairData.header		= sqlQuery.value( sqlQuery.record().indexOf("header")).toString() ;
		repairData.desc			= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		repairData.dtsCreated	= sqlQuery.value( sqlQuery.record().indexOf("dts_create")).toString() ;
		repairData.dtsUpdated	= sqlQuery.value( sqlQuery.record().indexOf("dts_update")).toString() ;
		ret.push_back( repairData );
	}

	return ret;
}

//
//
//

QVector<Database::RepairData> Database::getRepairsByCarId(const quint32 carId)
{
	QVector<Database::RepairData> ret;

	if ( !sqlQuery.exec( reqGetRepairByCarId.arg(carId) ))
	{
		emit critical( reqGetRepairByCarId.arg(carId) + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		Database::RepairData repairData;
		repairData.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		repairData.carId		= sqlQuery.value( sqlQuery.record().indexOf("carId")).toUInt() ;
		repairData.header		= sqlQuery.value( sqlQuery.record().indexOf("header")).toString() ;
		repairData.desc			= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		repairData.dtsCreated	= sqlQuery.value( sqlQuery.record().indexOf("dts_create")).toString() ;
		repairData.dtsUpdated	= sqlQuery.value( sqlQuery.record().indexOf("dts_update")).toString() ;
		ret.push_back( repairData );
	}

	return ret;
}

//
//
//

Database::RepairData Database::getRepairsById(const quint32 repairId)
{
	Database::RepairData ret;

	if ( !sqlQuery.exec( reqGetRepairById.arg(repairId) ))
	{
		emit critical( reqGetRepairById.arg(repairId) + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	if (sqlQuery.first())
	{
		ret.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		ret.carId		= sqlQuery.value( sqlQuery.record().indexOf("carId")).toUInt() ;
		ret.header		= sqlQuery.value( sqlQuery.record().indexOf("header")).toString() ;
		ret.desc		= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		ret.dtsCreated	= sqlQuery.value( sqlQuery.record().indexOf("dts_create")).toString() ;
		ret.dtsUpdated	= sqlQuery.value( sqlQuery.record().indexOf("dts_update")).toString() ;
	}

	return ret;
}

//
//
//

quint32 Database::getRepairsNumberByCarId(const quint32 carId)
{
	quint32 ret{0};

	if ( !sqlQuery.exec( reqGetRepairsNumberByCarId.arg(carId) ))
	{
		emit critical( reqGetRepairsNumberByCarId.arg(carId) + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		ret	= sqlQuery.value( sqlQuery.record().indexOf("COUNT(*)")).toUInt() ;
	}

	return ret;
}

//
//
//

void Database::deleteRepairById(const quint32 id)
{

	if ( !sqlQuery.exec( reqDeleteRepairById.arg(id) ) )
	{
		emit critical( reqDeleteRepairById.arg(id) + "\n\n" + sqlQuery.lastError().text() );
	}
}

//
//
//


void Database::deletePhotoById( const quint32 photoId )
{
	if ( !sqlQuery.exec( reqDeletePhotoById.arg(photoId) ) )
	{
		emit critical( reqDeletePhotoById.arg(photoId) + "\n\n" + sqlQuery.lastError().text() );
	}
}

//
//
//

quint32 Database::saveNewRapair(const Database::RepairData & repairData)
{
	quint32 ret{0};

	if ( !sqlQuery.exec( reqSaveNewRepair.arg( repairData.carId ).arg( repairData.header ).arg( repairData.desc ) ) )
	{
		emit critical( reqSaveNewRepair.arg( repairData.carId ).arg( repairData.header ).arg( repairData.desc ) + \
					   "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	if ( !sqlQuery.exec( reqGetLastInsertRowId ) )
	{
		emit critical( reqGetLastInsertRowId + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}


	;

	while (sqlQuery.next())
	{
		ret	= sqlQuery.value( sqlQuery.record().indexOf("last_insert_rowid()")).toUInt() ;
	}

	return ret;
}

//
//
//

bool Database::updateRepairData( const Database::RepairData & repairData )
{
	QString req = reqUpdateRepairById.arg( repairData.header )
									.arg( repairData.desc )
									.arg( repairData.id );

	if ( !sqlQuery.exec(req) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
		return false;
	}

	return true;
}

//
//
//

void Database::saveNewPhotoData(const Database::PhotoData & photoData)
{
	QSqlQuery mySqlQuery;

	mySqlQuery.prepare( reqSaveNewPhotoWithHolders );
	mySqlQuery.bindValue(":1", photoData.repairId );
	mySqlQuery.bindValue(":2", photoData.desc );
	mySqlQuery.bindValue(":3", photoData.rawData );

	if ( !mySqlQuery.exec() )
	{
		emit critical( sqlQuery.lastError().text() );
	}
}

//
//
//

void Database::updatePhotoData(const Database::PhotoData & photoData)
{
	QString req = reqUpdatePhotoById.arg( photoData.desc ).arg( photoData.id );

	if ( !sqlQuery.exec(req) )
	{
		emit critical( req + "\n\n" + sqlQuery.lastError().text() );
	}
}

//
//
//

quint32 Database::getPhotoesNumberByRepairId(const quint32 repairId)
{
	quint32 ret{0};

	if ( !sqlQuery.exec( reqGetPhotoesNumberByRepairId.arg(repairId) ))
	{
		emit critical( reqGetPhotoesNumberByRepairId.arg(repairId) + "\n\n" + sqlQuery.lastError().text() );
		return ret;
	}

	while (sqlQuery.next())
	{
		ret	= sqlQuery.value( sqlQuery.record().indexOf("COUNT(*)")).toUInt() ;
	}

	return ret;
}

//
//
//

void Database::getPhotoesDataByRepairId(const quint32 repairId, QVector<Database::PhotoData> & retValue)
{
	if ( !sqlQuery.exec( reqGetPhotoesByRepairId.arg(repairId) ))
	{
		emit critical( reqGetPhotoesByRepairId.arg(repairId) + "\n\n" + sqlQuery.lastError().text() );
		return;
	}

	while (sqlQuery.next())
	{
		Database::PhotoData photoData;
		photoData.id			= sqlQuery.value( sqlQuery.record().indexOf("id")).toUInt() ;
		photoData.repairId		= sqlQuery.value( sqlQuery.record().indexOf("repairId")).toUInt() ;
		photoData.desc			= sqlQuery.value( sqlQuery.record().indexOf("desc")).toString() ;
		photoData.rawData		= sqlQuery.value( sqlQuery.record().indexOf("data")).toByteArray() ;

		retValue.push_back( photoData );
	}

	return;
}
